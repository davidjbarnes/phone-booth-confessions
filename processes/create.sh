#!/bin/bash

fileName="confession_"$RANDOM$RANDOM".wav";
arecord -D plughw:0 --duration=10 -f cd ./audio/$fileName;

echo $fileName;