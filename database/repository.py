import sqlite3

conn = sqlite3.connect('database/phoneBooth.db')
curs = conn.cursor()

class Repository:
    def __init__(self):
        return

    class Setup:
        def __init__(self):
            return

        def get(self):
            for item in curs.execute("SELECT * FROM setup WHERE setupId=1"):
                print ""
            return item

    class Confession:
        def __init__(self):
            return

        def create(self, confession):
            curs.execute("INSERT INTO confession(timestamp,active,fileName) VALUES (?,?,?)", confession)
            conn.commit()
            for item in curs.execute("SELECT * FROM confession ORDER BY confessionId DESC LIMIT 1"):
                print ""
            return item

        def getRandom(self):
            for fileName in curs.execute("SELECT * FROM confession WHERE active=1 ORDER BY RANDOM() LIMIT 1"):
                print ""
            return fileName

        def update(self, confessionId, active):
            curs.execute("UPDATE confession SET active=? WHERE confessionId=?", (active,confessionId))
            conn.commit()
            return True

    #conn.close()