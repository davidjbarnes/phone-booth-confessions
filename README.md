# Phonebooth Confessions #

![2015-08-20 06.37.36.jpg](https://bitbucket.org/repo/aX7bRg/images/1071764964-2015-08-20%2006.37.36.jpg)

The Objective was to create a system which allowed bar patrons the opportunity to 1) record a confession and 2) hear a random confession. An old Bell phone was selected as the interface. Rotary was used as menu and primary user input. Handset was playback and audio input.

A Raspberry Pi was used as the CPU. Python, Linux, Bash, and SQLite are the core technologies used.

## Tutorials and More Info ##

Here you'll find a bit more info: http://www.fleshandmachines.com/2015/09/raspberry-pi-python-subprocess-and.html

## Images ##

Be sure to checkout the "[Downloads](https://bitbucket.org/davidjbarnes/phone-booth-confessions/downloads)" section for tons of photos of the booth, phone, and more.

## Contact ##

Feel free to contact me for more info:

David (me): nullableint@gmail.com