#!/usr/bin/python3
import RPi.GPIO as GPIO
import math
import subprocess

#variables
pulse_count = 0
rotary_done = True

#pin assignments
pin_pulse = 2
pin_switch = 3
pin_hook = 4

#GPIO setup
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(pin_switch, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(pin_pulse, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(pin_hook, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)


def hook(channel):
    print "Hook is: " + str(GPIO.input(pin_hook))
    if GPIO.input(pin_hook) == GPIO.HIGH:
        print "Play menu..."


def pulse_counter(channel):
    global pulse_count, rotary_done
    if GPIO.input(pin_switch) == GPIO.HIGH:
        pulse_count += 1
    else:
        rotary_done = not rotary_done


def rotary_switch(channel):
    global rotary_done
    print "rotary_switch state: " + str(GPIO.input(pin_switch))
    rotary_done = not rotary_done


print "CTRL+C to Halt"

while True:
    try:
        #events
        GPIO.add_event_detect(pin_hook, GPIO.BOTH, callback=hook, bouncetime=300)
        GPIO.add_event_detect(pin_switch, GPIO.BOTH, callback=rotary_switch, bouncetime=300)
        GPIO.add_event_detect(pin_pulse, GPIO.BOTH, callback=pulse_counter, bouncetime=5)
    except KeyboardInterrupt:
        GPIO.cleanup()
        exit()

GPIO.cleanup()