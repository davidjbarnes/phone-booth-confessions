#!/usr/bin/python3
import RPi.GPIO as GPIO
import math
import subprocess
import sqlite3
import time

#variables
count = 0
last = 1
mainMenuFileName = ""
conn = sqlite3.connect('database/phoneBooth.db')
curs = conn.cursor()

#pin assignments
pin_pulse = 2
pin_switch = 3
pin_hook = 4

#GPIO setup
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(pin_switch, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(pin_pulse, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(pin_hook, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)


def get_inactive_confession():
    for confession in curs.execute("SELECT * FROM confession WHERE active=0 ORDER BY RANDOM() LIMIT 1"):
        print ""
    return confession


def get_random_confession():
    for confession in curs.execute("SELECT * FROM confession WHERE active=1 ORDER BY RANDOM() LIMIT 1"):
        print ""
    return confession


def create_confession(new_confession):
    curs.execute("INSERT INTO confession(timestamp,active,fileName) VALUES (?,?,?)", new_confession)
    conn.commit()
    for item in curs.execute("SELECT * FROM confession ORDER BY confessionId DESC LIMIT 1"):
        print ""
    return item


def hook(channel):
    print "Hook is: " + str(GPIO.input(pin_hook))
    if GPIO.input(pin_hook) == GPIO.HIGH:
        audio = subprocess.check_output("./processes/play.sh ./audio/menu.wav", shell=True)
        print audio


def pulse_counter(channel):
    global count
    count += 1


def process_command(n):
    print "Command: " + str(n)
    if n == 1:
        #play the prompt for command1
        prompt = subprocess.check_output("./processes/play.sh ./audio/menu_command1.wav", shell=True)
        print prompt

        #create/record new confession | set to inactive by default
        file_name = subprocess.check_output("./processes/create.sh", shell=True)
        new_confession = create_confession((time.strftime("%c"), 0, file_name))
        print new_confession
    elif n == 2:
        #play the prompt for command2
        prompt = subprocess.check_output("./processes/play.sh ./audio/menu_command2.wav", shell=True)
        print prompt

        #play a random (active) confession
        random_confession = get_random_confession()
        random_confession_process = subprocess.check_output("./processes/play.sh ./audio/" + random_confession[2], shell=True)
        print random_confession_process
    elif n == 3:
        #play an in active confession
        inactive_confession = get_inactive_confession()
        inactive_confession_process = subprocess.check_output("./processes/play.sh ./audio/" + inactive_confession[2], shell=True)
        print inactive_confession_process
    else:
        print "^^^Unknown command"

#events
GPIO.add_event_detect(pin_hook, GPIO.BOTH, callback=hook, bouncetime=300)
GPIO.add_event_detect(pin_switch, GPIO.BOTH)

#main program
print "CTRL+C to Halt"

while True:
    try:
        #wait for rotary dial IRQ
        if GPIO.event_detected(pin_switch) and GPIO.input(pin_hook) == GPIO.HIGH:
            current = GPIO.input(pin_switch)
            if last != current:
                if current == 0:
                    GPIO.add_event_detect(pin_pulse, GPIO.BOTH, callback=pulse_counter, bouncetime=5)
                else:
                    GPIO.remove_event_detect(pin_pulse)
                    number = math.floor(count/2)
                    count = 0
                    process_command(number)
                last = GPIO.input(pin_switch)
    except KeyboardInterrupt:
        GPIO.cleanup()
        exit()

GPIO.cleanup()