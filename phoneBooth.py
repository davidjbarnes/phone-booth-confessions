#!/usr/bin/env python

import RPi.GPIO as GPIO
import time
#import math, sys, os
import subprocess
from models.confession import Confession
from models.setup import Setup

pinHook = 24
pinLED = 25

#setup GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(pinHook, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(pinLED, GPIO.OUT)
GPIO.add_event_detect(pinHook, GPIO.RISING)

#Models
confession = Confession()
setup = Setup()

#Globals
_menu = ""

######################################################################

#Get setup stuff every time hook is low
_menu = setup.get()[1]

while True:
    #tempHook = raw_input("Simulate hook high/low (1=High or 0=Low): ")

    if GPIO.event_detected(pinHook):
        if GPIO.input(pinHook) == GPIO.HIGH:
            #The hook is high, play menu audio
            audioReturn = subprocess.check_output("./processes/playAudio.sh ./audio/"+_menu, shell=True)
            print(audioReturn)

            #The audio menu prompt
            command = raw_input("Simulate rotary command (1=New Confession, 2=Random Confession): ")

            #New Confession command
            if command == "1":
                fileName = subprocess.check_output("./processes/createConfession.sh", shell=True)
                newConfession = confession.create((time.strftime("%c"), 0, fileName))
                print(newConfession)

            #Play random confession command
            elif command == "2":
                randomConfession = confession.getRandom()
                audioReturn = subprocess.check_output("./processes/playAudio.sh ./audio/"+randomConfession[2], shell=True)
                print(audioReturn)

            elif command == "update":
                confessionId = raw_input('Enter confessionId: ')
                active = raw_input('Active (1 or 0): ')
                confession.update(confessionId, active)

            elif command == "exit":
                break

            else:
                break
        else:
            #Get setup stuff every time hook is low
            _menu = setup.get()[1]

#gpio.cleanup()